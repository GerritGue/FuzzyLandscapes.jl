using FuzzyLandscapes
using ArchGDAL
using Interpolations
using Test

@testset "Trapezoidal_MF" begin
    x = 5
    output = Trapezoidal_MF(x, 1, 5, 7, 8)
    expected_1 = 1

    @test expected_1 == output
end

@testset "Triangular_MF" begin
    x = 5
    output = Triangular_MF(x, 3, 6, 8)
    output = round(output, digits = 4)
    expected = 0.6667

    @test expected == output
end

@testset "Gaussian_MF" begin
    x = 5
    output = Gaussian_MF(x, 5, 2)
    expected = 1

    @test expected == output
end

@testset "Gaussian2_MF" begin
    x = 5
    output = Gaussian2_MF(x, 4, 2, 8, 1)
    expected = 1

    @test expected == output
end

@testset "Generalised_bell_MF" begin
    x = 5
    output = Generalised_bell_MF(x, 2, 4, 6)
    output = round(output, digits = 4)
    expected = 0.9961

    @test expected == output
end

@testset "Sigmoid_MF" begin
    x = 5
    output = Sigmoid_MF(x, 2, 4)
    output = round(output, digits = 4)
    expected = 0.8808

    @test expected == output
end

@testset "p_Sigmoid_MF" begin
    x = 5
    output = p_Sigmoid_MF(x, 2, 3, -5, 8)
    output = round(output, digits = 4)
    expected = 0.9820

    @test expected == output
end

@testset "d_Sigmoid_MF" begin
    x = 5
    output = d_Sigmoid_MF(x, 5, 2, 5, 7)
    output = round(output, digits = 4)
    expected = 1.0

    @test expected == output
end

@testset "Z_MF" begin
    x = 5
    output = Z_MF(x, 3, 7)
    output = round(output, digits = 4)
    expected = 0.5000

    @test expected == output
end

@testset "S_MF" begin
    x = 5
    output = S_MF(x, 1, 8)
    output = round(output, digits = 4)
    expected = 0.6327

    @test expected == output
end

@testset "PI_MF" begin
    x = 5
    output = PI_MF(x, 1, 4, 5, 10)
    output = round(output, digits = 4)
    expected = 1

    @test expected == output
end

@testset "or" begin

    input_1_or = fill(1.0, 10, 10) .- 0.25

    input_2_or = fill(1.0, 10, 10)
    
    output_matrix_or = zeros(10, 10)
    
    output = or(input_1_or, input_2_or, output_matrix_or)
    
    expected = input_2_or
    
    @test expected == output
end

@testset "and" begin

    input_1_and = fill(1.0, 10, 10) .- 0.25

    input_2_and = fill(1.0, 10, 10)
    
    output_matrix_and = zeros(10, 10)
    
    output = and(input_1_and, input_2_and, output_matrix_and)
    
    expected = input_1_and
    
    @test expected == output

end

@testset "prob_or" begin

    input_1_Prob_or = fill(1.0, 10, 10) .- 0.25

    input_2_Prob_or = fill(1.0, 10, 10)

    output = prob_or(input_1_Prob_or, input_2_Prob_or)

    expected = input_2_Prob_or

    @test expected == output
end

@testset "centroid" begin
    input_1_centroid = reduce(hcat, [[j, j, j, j] for j in 1:4])

    output_centroid = zeros(4, 4)
    
    for i in eachindex(output_centroid)
        output_centroid[i] = Trapezoidal_MF(input_1_centroid[i], 0, 2, 3, 5)
    end
    
    output = centroid(input_1_centroid, output_centroid)
    
    expected = 2.5
    
    @test expected == output
    
end

@testset "mom" begin

    input_1_mom = reduce(hcat, [[j, j, j] for j in 1:3])

    output_mom = zeros(3, 3)
    
    for i in eachindex(output_mom)
        output_mom[i] = Trapezoidal_MF(input_1_mom[i], 0, 1, 2, 3)
    end
    
    output = mom(input_1_mom, output_mom)
    
    expected = 1.5
    
    @test expected == output
end

@testset "som" begin
    input_1_som = reduce(hcat, [[j, j, j] for j in 1:3])

    output_som = zeros(3, 3)
    
    for i in eachindex(output_som)
        output_som[i] = Trapezoidal_MF(input_1_som[i], 0, 1, 2, 3)
    end
    
    output = som(input_1_som, output_som)
    
    expected = 1
    
    @test expected == output
end

@testset "lom" begin
    input_1_lom = reduce(hcat, [[j, j, j] for j in 1:3])

    output_lom = zeros(3, 3)
    
    for i in eachindex(output_lom)
        output_lom[i] = Trapezoidal_MF(input_1_lom[i], 0, 1, 2, 3)
    end
    
    output = lom(input_1_lom, output_lom)
    
    expected = 2
    
    @test expected == output
end

@testset "bisector" begin
    input_1_bisector = reduce(hcat, [[j, j, j] for j in 1:3])

    output_bisector = zeros(3, 3)
    
    for i in eachindex(output_bisector)
        output_bisector[i] = Trapezoidal_MF(input_1_bisector[i], 0, 1, 2, 3)
    end
    
    output = bisector(input_1_bisector, output_bisector)

    expected = 1.00

    @test expected == output
end

@testset "and_result" begin
    input_and_result_1 = reduce(hcat, [[j, j, j, j] for j in 1:4]) .- 0.25
    input_and_result_2 = reduce(hcat, [[j, j, j, j] for j in 1:4])
    input_and_result_3 = reduce(hcat, [[j, j, j, j] for j in 1:4]) .- 0.5
    output = and_result(input_and_result_2, input_and_result_1, input_and_result_3)
    
    expected = input_and_result_3
    
    @test expected == output
end

@testset "frbs" begin
    slope_matrix_test = fill(0.0, (10, 10))

    for i in eachindex(slope_matrix_test)
        slope_matrix_test[i] = collect(1.0:100.0)[i]
    end
    
    flat = fuzzy_classification(slope_matrix_test, :Trapezoidal, 0, 2.5, 5.0, 7.5)
    
    gentle = fuzzy_classification(slope_matrix_test, :Trapezoidal, 10, 12.5, 15.0, 17.5)
    
    flat_or_gentle = frbs(flat.Fuzzified_Matrix, gentle.Fuzzified_Matrix, :Or, flat.Source_Matrix, :Bisector)
    
    expected = flat_or_gentle.Fuzzified_Matrix[1, 1]
    
    @test expected == 0.4f0
end

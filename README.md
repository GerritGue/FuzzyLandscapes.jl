# FuzzyLandscapes.jl

The goal of this package is the:

- Fuzzification of crisp raster data or 2 dimensional arrays
- Rule-based combination of fuzzy datasets
- Defuzzification of those combined datasets
- Export of the fuzzified datasets

The package uses 2 dimensional arrays as a basis. Thus, you can either fuzzify matrices or datasets read via Rasters.jl. But you can also make use of ArchGDAL datasets or use the path of the raster as an input.

### Installation

To install the current version of FuzzyLandscapes.jl from GitLab:

   ```
   julia> using Pkg
   julia> Pkg.add(url = "https://gitlab.com/GerritGue/FuzzyLandscapes.jl.git")

   ```
   
   Have a look at the Pluto notebook in the `examples` folder for a basic introduction into fuzzy logic in general and into the basic functionalities of the package.


### Acknowledgements

The package was developed by Ilmar Leimann, Gerrit Günther and Daniel Knitter. It is currently maintained and developed by Gerrit Günther.

### Citation

If you use this package in a publication, or simply want to refer to it, please cite our zenodo record:

```
@software{fuzzylandscapes2024,
  author       = {Günther, Gerrit and
                  Leimann, Ilmar and
                  Knitter, Daniel},
  title        = {{FuzzyLandscapes.jl -- Fuzzy analyses with a focus 
                   on raster data}},
  month        = nov,
  year         = 2024,
  publisher    = {Zenodo},
  version      = {0.2.0},
  doi          = {10.5281/zenodo.14044038},
  url          = {https://doi.org/10.5281/zenodo.14044038}
}

```
"""
# Centroid of the area under the output fuzzy set. 
    
    This function returns a defuzzified value of a membership function mf positioned at associated variable value x

    x: the input data
     
    mfx: membership function

    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

# Example

    ```julia-repl

        output = centroid(input_matrix, fuzzy_matrix; missing_input)

    ```
"""
function centroid(x::AbstractMatrix{<:Real}, mfx::Matrix; missing_input::Any = nothing)

    if any(isnan, mfx)
        total_area = sum(filter(!isnan, mfx))
        out = sum((filter(!isnan, mfx .* x)) / total_area)
    elseif missing_input in mfx

        total_area = sum(filter(x -> x !== missing_input, mfx))

        out = sum((total_area .* x) / total_area)

    else 
        total_area = sum(mfx)
        out = sum(mfx .* x) / total_area
    end

    return out
end

export centroid


"""
# Mean of the values for which the output fuzzy set is maximum. 
    
    This function returns a defuzzified value of a membership function mf positioned at associated variable value x

    x: the input data
      
    mfx: membership function

# Example

    ```julia-repl

        output = mom(input_matrix, fuzzy_matrix)

    ```
"""
function mom(x::AbstractMatrix{<:Real}, mfx::Matrix)
    
    filter_data = maximum(filter(!isnan, mfx))
    
    index = findall(x -> x == filter_data, mfx)
    
    values = x[index]
    
    return Statistics.mean(values)
    
end

export mom


"""
# Smallest value for which the output fuzzy set is maximum

    This function returns a defuzzified value of a membership function mf positioned at associated variable value x

    x: the input data
      
    mfx: membership function

# Example
    
    ```julia-repl

        output = som(input_matrix, fuzzy_matrix)
    ```
"""
function som(x::AbstractMatrix{<:Real}, mfx::Matrix)

    filter_data = maximum(filter(!isnan, mfx))

    index = findall(x -> x == filter_data, mfx)

    values = x[index]

    ind = argmin(abs.(values))

    return values[ind]

end

export som


"""
# Largest value for which the output fuzzy set is maximum
    
    This function returns a defuzzified value out, of a membership function mf positioned at associated variable value x

    x: the input data
      
    mfx: membership function

# Example

    ```julia-repl

        output = lom(input_matrix, fuzzy_matrix)

    ```
"""
function lom(x::AbstractMatrix{<:Real}, mfx::Matrix)

    filter_data = maximum(filter(!isnan, mfx))

    index = findall(x -> x == filter_data, mfx)

    values = x[index]

    ind = argmax(abs.(values))

    return values[ind]
end

export lom


"""
# Bisector of the area under the output fuzzy set
    
    This funtion returns a defuzzified value out, of a membership function mf positioned at associated variable value x

	x: the input data

    mfx: membership function

    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

# Example

    ```julia-repl

        output = bisector(input_matrix, fuzzy_matrix; missing_input)

    ```
"""
function bisector(x::AbstractMatrix{<:Real}, mfx::Matrix; missing_input::Any = nothing)
    tmp = 0
    filter_mfx = filter(!isnan, mfx)
    filter_x = filter(!isnan, x)

    if !isnothing(missing_input)
        filter_mfx !== maximum(filter(x -> x == missing_input, filter_mfx))
        filter_x !== filter(x -> x == missing_input, filter_x)
    end


    total_area = sum(filter_mfx)
    out = 0

    for k in eachindex(filter_mfx)
        tmp += filter_mfx[k]
        if tmp >= total_area / 2
            break
        end
        return out = filter_x[k]
    end

    return out
end

export bisector

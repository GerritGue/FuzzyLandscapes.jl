# This function evaluates / classifies a dataset with fuzzy logic and also exports the results as .tif file.

"""
    This function evaluates / classifies a dataset with fuzzy logic and also exports the results as .tif file.

    fuzzy_classification(input::String, 
                         output::String, method::Symbol, 
                         x1::Any = nothing, x2::Any = nothing,
                         x3::Any = nothing, x4::Any = nothing; 
                         missing_input::Any = nothing, missing_output::Any = nothing)
    
    input: your input data, for example your location of tif file "../example.tif"
    
    output: path and name for your output .tif file "../example.tif"

    method: choose your method of membership function for further information look into membership_functions.jl. 
            Current possibilities are: :Generalised_bell
                                       :Trapezoidal
                                       :Triangular
                                       :Gaussian
                                       :Gaussian2
                                       :Sigmoid
                                       :D_Sigmoid
                                       :Z
                                       :S
                                       :PI
    
    x1,x2,x3,x4: parameters of membership function for further information look into membership_functions.jl

    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

    missing_output: Define a 'missing_output' value to set the missing value in the fuzzified set. 

# Example    

    ```julia-repl
        gentle = FuzzyLandscapes.fuzzy_classification(
            datadir("derived_data/slope.tif"),
            datadir("derived_data/fuzzy_slope_bawue.tif"),
            :Z,
            5,
            10; 
            missing_input = -9999, 
            missing_output = 0)
    ```
"""
function fuzzy_classification(input::String, output::String, fuzzy_method::Symbol,
                             x1::Any = nothing, x2::Any = nothing, x3::Any = nothing,
                             x4::Any = nothing; missing_input::Any = nothing,
                             missing_output::Any = nothing)

    input_matrix, input_raster = FuzzyLandscapes.raster_to_matrix(input)

    fuzzy_matrix::Matrix = zeros(Float32, size(input_matrix))

    fuzzified_matrix::Matrix = FuzzyLandscapes.fuzzification(input_matrix, fuzzy_matrix, fuzzy_method, x1,
                                                    x2, x3, x4; missing_input, missing_output)

    if !isempty(fuzzified_matrix)

        if !isnothing(missing_output)
            FuzzyLandscapes.write_raster(fuzzified_matrix,
                                     input_raster,
                                     output;
                                     missing_output)
        else
            FuzzyLandscapes.write_raster(fuzzified_matrix,
                                     input_raster,
                                     output)
        end
        
        return (Fuzzified_Matrix = fuzzified_matrix, 
                Source_Matrix = input_matrix, 
                Source_Raster = input_raster)

    else
        @error "Fuzzification unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

# This function evaluates / classifies a dataset with fuzzy logic, but does not export the results as .tif file.

"""
    This function evaluates / classifies a dataset with fuzzy logic, but does not export the results as .tif file.

    fuzzy_classification(input::String, method::Symbol,
                         x1::Any = nothing, x2::Any = nothing, x3::Any = nothing,
                         x4::Any = nothing; missing_input::Any = nothing,
                         missing_output::Any = nothing)


    input: your input data, for example your location of tif file "../example.tif"
    
    method: choose your method of membership function for further information look into membership_functions.jl. 
            Current possibilities are: :Generalised_bell
                                       :Trapezoidal
                                       :Triangular
                                       :Gaussian
                                       :Gaussian2
                                       :Sigmoid
                                       :D_Sigmoid
                                       :Z
                                       :S
                                       :PI

    x1,x2,x3,x4: parameters of membership function for further information look into membership_functions.jl

    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

    missing_output: Define a 'missing_output' value to set the missing value in the fuzzified set. 

# Example

    ```julia-repl
        gentle = FuzzyLandscapes.fuzzy_classification(
            datadir("derived_data/slope.tif"),
            :Z,
            5,
            10; 
            missing_input = -9999, 
            missing_output = 0)
    ```
"""
function fuzzy_classification(input::String, fuzzy_method::Symbol,
                             x1::Any = nothing, x2::Any = nothing, x3::Any = nothing,
                             x4::Any = nothing; missing_input::Any = nothing,
                             missing_output::Any = nothing)

    input_matrix, input_raster = FuzzyLandscapes.raster_to_matrix(input)

    fuzzy_matrix::Matrix{eltype(input_matrix)} = zeros(eltype(input_matrix), size(input_matrix))

    fuzzified_matrix = FuzzyLandscapes.fuzzification(input_matrix, fuzzy_matrix, fuzzy_method, x1,
                                                    x2, x3, x4; missing_input, missing_output)

    if !isempty(fuzzified_matrix)

        return (Fuzzified_Matrix = fuzzified_matrix, 
                Source_Matrix = input_matrix, 
                Source_Raster = input_raster)
    else
        @error "Fuzzification unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

# evaluation / classify your data with fuzzy, without export

"""
    This function evaluates / classifies a 2-dimensional array dataset with fuzzy logic.

    fuzzy_classification(input::AbstractMatrix{<:Real}, method::Symbol,
                         x1::Any = nothing, x2::Any = nothing, x3::Any = nothing,
                         x4::Any = nothing; missing_input::Any = nothing,
                         missing_output::Any = nothing)
    
    input: your input data, for example your location of tif file "../example.tif"
    
    fuzzy_method: choose your method of membership function for further information look into membership_functions.jl. 
            Current possibilities are: :Generalised_bell
                                       :Trapezoidal
                                       :Triangular
                                       :Gaussian
                                       :Gaussian2
                                       :Sigmoid
                                       :D_Sigmoid
                                       :Z
                                       :S
                                       :PI

    x1,x2,x3,x4: parameters of membership function for further information look into membership_functions.jl

    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

    missing_output: Define a 'missing_output' value to set the missing value in the fuzzified set. 

# Example

    ```julia-repl
        Close_and_flat = fuzzy_classification(
            input_matrix,
            :Z,
            5,
            10; 
            missing_input = -9999, 
            missing_output = 0)
    ```                          
"""
function fuzzy_classification(input::AbstractMatrix{<:Real}, fuzzy_method::Symbol,
                             x1::Any = nothing, x2::Any = nothing, x3::Any = nothing,
                             x4::Any = nothing; missing_input::Any = nothing,
                             missing_output::Any = nothing)

    fuzzy_matrix::Matrix{eltype(input)} = zeros(eltype(input), size(input))

    fuzzified_matrix = FuzzyLandscapes.fuzzification(input, fuzzy_matrix, fuzzy_method, x1,
                                                    x2, x3, x4; missing_input, missing_output)

    if !isempty(fuzzified_matrix)

        return (Fuzzified_Matrix = fuzzified_matrix, 
                Source_Matrix = input)
    else
        @error "Fuzzification unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

export fuzzy_classification

# Fuzzy Rule Based System (frbs), without export

"""
    This function creates a Fuzzy Rule Based System (frbs). The input parameters are used to evaluate / fuzzify the data in terms of and/or. Finally, the data is exported as a ".tif".

    for example, after you classified your data (fuzzy_classification),
        => if "flat" and/or "dry", then class_1
        "flat" and/or "dry", then (defuzzy) class_1

    input_1: classified data, e.g. "flat"
    
    input_2: classified data, e.g. "dry"

    fuzzy_method: choose your method for further information look into fuzzy_connection.jl
            Current possibilities are: :And
                                      :Or

    source_matrix: source_matrix, which is used for defuzzification, for example, if you derived "flat"/"steep" from "slope" data,
                 then usually "slope" data is used as input_matrix. If you already used "fuzzy_classification" function, 
                 then you can use: flat["source_matrix"]
    
    defuzzy_method: choose your method of defuzzification for further information look into centroid_bisector_lom_som_mom.jl
                    Current possibilities are: :Centroid
                                               :Bisector
                                               :Mom
                                               :Som
                                               :Lom
# Example

    ```julia-repl
        Close_and_flat = frbs(flat.Fuzzified_Matrix, close.Fuzzified_Matrix, 
                              :And, 
                              close.Source_Matrix,
                              :Som)
    ```                                              
"""
function frbs(input_1::AbstractMatrix{<:Real}, input_2::AbstractMatrix{<:Real}, fuzzy_method::Symbol, source_matrix::AbstractMatrix{<:Real},
              defuzzy_method::Symbol; missing_input::Any = nothing,
              missing_output::Any = nothing)

    fuzzy_output = FuzzyLandscapes.fuzzy_connection(input_1, input_2, fuzzy_method; missing_input, missing_output)

    if !isempty(fuzzy_output)  

        defuzzy = FuzzyLandscapes.defuzzy(source_matrix, fuzzy_output,
                                          defuzzy_method; missing_input)

        if fuzzy_method == :And
            if defuzzy_method == :Centroid

                return (Rule = :And_Centroid, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Bisector

                return (Rule = :And_Bisector, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Mom

                return (Rule = :And_Mom, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Som

                return (Rule = :And_Som, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

                    elseif defuzzy_method == :Lom

                return (Rule = :And_Lom, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)
            end

        elseif fuzzy_method == :Or
            if defuzzy_method == :Centroid

                return (Rule = :Or_Centroid, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Bisector

                return (Rule = :Or_Bisector, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Mom

                return (Rule = :Or_Mom, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Som

                return (Rule = :Or_Som, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)


            elseif defuzzy_method == :Lom

                return (Rule = :Or_Lom, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)
            end
        end
    else
        @error "Creation of Fuzzy Rule Based System unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

# Fuzzy Rule Based System (frbs) with export

"""
    This function creates a Fuzzy Rule Based System (frbs). The input parameters are used to evaluate / fuzzify the data in terms of and/or.

    for example, after you classified your data (fuzzy_classification),
        => if "flat" and/or "dry", then class_1
            "flat" and/or "dry", then (defuzzy) class_1

    input_1: classified data, e.g. "flat"
    
    input_2: classified data, e.g."dry"
    
    template_raster: path and name for the template raster used for the raster creation
    
    output: path and name for your output .tif file "../example.tif"

    fuzzy_method: choose your method for further information look into fuzzy_connection.jl
            Current possibilities are: :And
                                       :Or

    source_matrix: source_matrix, which is used for defuzzification, for example, if you derived "flat"/"steep" from "slope" data, then usually "slope" data is used as input_matrix. If you already used "fuzzy_classification" function, then you can use: flat.Source_Matrix
    
    defuzzy_method: choose your method of defuzzification for further information look into centroid_bisector_lom_som_mom.jl
                    Current possibilities are: :Centroid
                                               :Bisector
                                               :Mom
                                               :Som
                                               :Lom
# Example

    ```julia-repl
        Close_and_flat = frbs(flat.Fuzzified_Matrix, close.Fuzzified_Matrix,, 
                              datadir("derived_data/distance_raster_clipped.tif"), 
                              datadir("derived_data/Close_and_flat.tif"), 
                              :And, 
                              close.Source_Matrix,
                              :Som;
                              missing_input,
                              missing_output)
    ```
"""
function frbs(input_1::AbstractMatrix{<:Real}, input_2::AbstractMatrix{<:Real}, template_raster::String, output::String, 
              fuzzy_method::Symbol, source_matrix::Any, defuzzy_method::Symbol; 
              missing_input::Any = nothing, missing_output::Any = nothing)

    template_raster = ArchGDAL.read(template_raster)

    fuzzy_output = FuzzyLandscapes.fuzzy_connection(input_1, input_2, fuzzy_method; missing_input, missing_output)

    if !isempty(fuzzy_output)

        defuzzy = FuzzyLandscapes.defuzzy(source_matrix, fuzzy_output,
                                          defuzzy_method; missing_input)

        if !isnothing(missing_output)
            FuzzyLandscapes.write_raster(fuzzy_output,
                                         template_raster,
                                         output;
                                         missing_output)
        else
            FuzzyLandscapes.write_raster(fuzzy_output,
                                         template_raster,
                                         output)
        end

        if fuzzy_method == :And
            if defuzzy_method == :Centroid

                return (Rule = :And_Centroid, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Bisector

                return (Rule = :And_Bisector, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Mom

                return (Rule = :And_Mom, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Som

                return (Rule = :And_Som, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

                    elseif defuzzy_method == :Lom

                return (Rule = :And_Lom, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)
            end

        elseif fuzzy_method == :Or
            if defuzzy_method == :Centroid

                return (Rule = :Or_Centroid, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Bisector

                return (Rule = :Or_Bisector, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Mom

                return (Rule = :Or_Mom, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Som

                return (Rule = :Or_Som, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)


            elseif defuzzy_method == :Lom

                return (Rule = :Or_Lom, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)
            end
        end
    else
        @error "Creation of Fuzzy Rule Based System unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

# Fuzzy Rule Based System (frbs) with export

"""
    This function creates a Fuzzy Rule Based System (frbs) using the in memory source raster of the Fuzzy Rule Based System directly. The input parameters are used to evaluate / fuzzify the data in terms of and/or.

    for example, after you classified your data (fuzzy_classification),
        => if "flat" and/or "dry", then class_1
            "flat" and/or "dry", then (defuzzy) class_1

    input_1: classified data, e.g. "flat"
    
    input_2: classified data, e.g."dry"
    
    template_raster: path and name for the template raster used for the raster creation
    
    output: path and name for your output .tif file "../example.tif"

    fuzzy_method: choose your method for further information look into fuzzy_connection.jl
            Current possibilities are: :And
                                       :Or

    source_matrix: source_matrix, which is used for defuzzification, for example, if you derived "flat"/"steep" from "slope" data, then usually "slope" data is used as input_matrix. If you already used "fuzzy_classification" function, then you can use: flat.Source_Matrix
    
    defuzzy_method: choose your method of defuzzification for further information look into centroid_bisector_lom_som_mom.jl
                    Current possibilities are: :Centroid
                                               :Bisector
                                               :Mom
                                               :Som
                                               :Lom
# Example

    ```julia-repl
        Close_and_flat = frbs(flat.Fuzzified_Matrix, close.Fuzzified_Matrix,, 
                              close.Source_Raster, 
                              datadir("derived_data/Close_and_flat.tif"), 
                              :And, 
                              close.Source_Matrix,
                              :Som;
                              missing_input,
                              missing_output)
    ```
"""
function frbs(input_1::AbstractMatrix{<:Real}, input_2::AbstractMatrix{<:Real}, template_raster::ArchGDAL.RasterDataset{Float32, ArchGDAL.IDataset}, 
              output::String, fuzzy_method::Symbol, source_matrix::Any, defuzzy_method::Symbol; 
              missing_input::Any = nothing, missing_output::Any = nothing)

    fuzzy_output = FuzzyLandscapes.fuzzy_connection(input_1, input_2, fuzzy_method; missing_input, missing_output)

    if !isempty(fuzzy_output)

        defuzzy = FuzzyLandscapes.defuzzy(source_matrix, fuzzy_output,
                                          defuzzy_method; missing_input)

        if !isnothing(missing_output)
            FuzzyLandscapes.write_raster(fuzzy_output,
                                         template_raster,
                                         output;
                                         missing_output)
        else
            FuzzyLandscapes.write_raster(fuzzy_output,
                                         template_raster,
                                         output)
        end

        if fuzzy_method == :And
            if defuzzy_method == :Centroid

                return (Rule = :And_Centroid, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Bisector

                return (Rule = :And_Bisector, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Mom

                return (Rule = :And_Mom, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Som

                return (Rule = :And_Som, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

                    elseif defuzzy_method == :Lom

                return (Rule = :And_Lom, 
                        Result_And = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)
            end

        elseif fuzzy_method == :Or
            if defuzzy_method == :Centroid

                return (Rule = :Or_Centroid, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Bisector

                return (Rule = :Or_Bisector, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Mom

                return (Rule = :Or_Mom, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)

            elseif defuzzy_method == :Som

                return (Rule = :Or_Som, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)


            elseif defuzzy_method == :Lom

                return (Rule = :Or_Lom, 
                        Result_Or = defuzzy, 
                        Fuzzified_Matrix = fuzzy_output)
            end
        end
    else
        @error "Creation of Fuzzy Rule Based System unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

export frbs



"""
# Export the calculated `fuzzy_matrix` into a `fuzzy_data.tif` using the source raster of the unfuzzified values as `template_raster.tif`. You also have to define a missing Value for the output raster using the 'missing_output' parameter.
    
    fuzzy_matrix: a fuzzy matrix

    template_raster: path to a template raster
    
    output_raster: path to the output raster 
    
    missing_output: missing value of the output raster; not set if not specified

# Example
    
    ```julia-repl
        write_raster(gentle.Fuzzified_Matrix,,
                     "slope.tif",
                     "fuzzy_data.tif";
                     missing_output = -3.4f38)
    ```
"""
function write_raster(fuzzy_matrix::Matrix{Float32}, 
                      template_raster::String, 
                      output_raster::String; 
                      missing_output::Any = nothing)

    template_raster = ArchGDAL.read(template_raster)

    ArchGDAL.create(output_raster;
                    driver = ArchGDAL.getdriver("GTiff"),
                    width = ArchGDAL.width(template_raster),
                    height = ArchGDAL.height(template_raster),
                    nbands = 1,
                    dtype = Float32) do raster_export
        ArchGDAL.setgeotransform!(raster_export,
                                  ArchGDAL.getgeotransform(template_raster))

        ArchGDAL.setproj!(raster_export,
                          ArchGDAL.getproj(template_raster))

        if missing_output !== nothing
            ArchGDAL.setnodatavalue!(ArchGDAL.getband(raster_export, 1), missing_output)
        end

        ArchGDAL.write!(raster_export,
                        convert(Matrix{Float32}, fuzzy_matrix),
                        1)
    end
end


"""
# Export the calculated `fuzzy_matrix` into a `output_raster.tif` using the source raster of the Fuzzy Rule Based System or the fuzzy_classification. You also have to define a missing Value for the output raster using the 'missing_output' parameter.

    fuzzy_matrix: a fuzzy matrix

    template_raster: a source raster from the fuzzified object
    
    output_raster: path to the output raster 
    
    missing_output: missing value of the output raster; not set if not specified


# Example
    ```julia-repl

        write_raster(gentle.Fuzzified_Matrix, 
                     gentle.Source_Raster, 
                     "fuzzified_slope.tif";
                      missing_output = -3.4f38)
```
"""
function write_raster(fuzzy_matrix::Matrix{Float32}, 
                      template_raster::ArchGDAL.IDataset, 
                      output_raster::String; 
                      missing_output::Any = nothing)
    
    ArchGDAL.create(output_raster;
                    driver = ArchGDAL.getdriver("GTiff"),
                    width = ArchGDAL.width(template_raster),
                    height = ArchGDAL.height(template_raster),
                    nbands = 1,
                    dtype = Float32) do raster_export
        ArchGDAL.setgeotransform!(raster_export,
                                  ArchGDAL.getgeotransform(template_raster))

        ArchGDAL.setproj!(raster_export,
                          ArchGDAL.getproj(template_raster))

        if missing_output !== nothing
            ArchGDAL.setnodatavalue!(ArchGDAL.getband(raster_export, 1), missing_output)
        end

        ArchGDAL.write!(raster_export,
                        convert(Matrix{Float32}, fuzzy_matrix),
                        1)
    end
end


"""
# Export the calculated `fuzzy_matrix` into a `output_raster.tif` using the source raster of the Fuzzy Rule Based System or the fuzzy_classification. You also have to define a missing Value for the output raster using the 'missing_output' parameter.

    fuzzy_matrix: a fuzzy matrix

    template_raster: path to a template raster
    
    output_raster: path to the output raster 
    
    missing_output: missing value of the output raster; not set if not specified


# Example

    ```julia-repl

        write_raster(gentle.Fuzzified_Matrix, 
                     gentle.Source_Raster, 
                     "fuzzified_slope.tif";
                      missing_output = -3.4f38)
    ```
"""
function write_raster(fuzzy_matrix::Matrix{Float32}, 
                      template_raster::ArchGDAL.RasterDataset{Float32, ArchGDAL.IDataset}, 
                      output_raster::String; 
                      missing_output::Any = nothing)
    
    ArchGDAL.create(output_raster;
                    driver = ArchGDAL.getdriver("GTiff"),
                    width = ArchGDAL.width(template_raster),
                    height = ArchGDAL.height(template_raster),
                    nbands = 1,
                    dtype = Float32) do raster_export
        ArchGDAL.setgeotransform!(raster_export,
                                  ArchGDAL.getgeotransform(template_raster))

        ArchGDAL.setproj!(raster_export,
                          ArchGDAL.getproj(template_raster))

        if missing_output !== nothing
            ArchGDAL.setnodatavalue!(ArchGDAL.getband(raster_export, 1), missing_output)
        end

        ArchGDAL.write!(raster_export,
                        convert(Matrix{Float32}, fuzzy_matrix),
                        1)
    end
end

export write_raster
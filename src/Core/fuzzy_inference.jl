"""
# "or" intersection of two fuzzy sets

      input_1: a fuzzy matrix

      input_2: a fuzzy matrix
      
      output_matrix: an empty matrix used to store the results of the evaluation
      
      missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

      missing_output: Define a 'missing_output' value to set the missing value in the fuzzified set. 

# Example

    ```julia-repl

        out = or(input_1, input_2, output_matrix; missing_input, missing_output)

    ```
"""
function or(input_1::AbstractMatrix{<:Real}, input_2::AbstractMatrix{<:Real}, output_matrix::Matrix; missing_input::Any = nothing,
    missing_output::Any = nothing)
   
    for i in eachindex(output_matrix)

        if any(isnan, input_1[i]) && any(isnan, input_2[i])
            output_matrix[i] = NaN
        elseif input_1[i] == missing_input && input_2[i] == missing_input
            output_matrix[i] = missing_output
        else
            if input_1[i] > input_2[i] || input_2[i] == NaN || input_2[i] == missing_input
                output_matrix[i] = input_1[i]
            elseif input_1[i] < input_2[i] || input_1[i] == NaN || input_1[i] == missing_input
                output_matrix[i] = input_2[i]
            elseif input_1[i] == input_2[i]
                output_matrix[i] = input_2[i]
            end
        end
    end

    return output_matrix

end

export or


"""
# "and" intersection of two fuzzy sets

    input_1: a fuzzy matrix

    input_2: a fuzzy matrix
      
    output_matrix: an empty matrix used to store the results of the evaluation
      
    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

    missing_output: Define a 'missing_output' value to set the missing value in the fuzzified set. 

# Example
    
    ```julia-repl

        out_1 = and(input_1, input_2, output_matrix; missing_input, missing_output)

    ```
"""
function and(input_1::AbstractMatrix{<:Real}, input_2::AbstractMatrix{<:Real}, output_matrix::Matrix; missing_input::Any = nothing,
    missing_output::Any = nothing)
    
    for i in eachindex(output_matrix)

        if any(isnan, input_1[i]) && any(isnan, input_2[i])
            output_matrix[i] = NaN
        elseif input_1[i] == missing_input && input_2[i] == missing_input
            output_matrix[i] = missing_output
        else
            if input_1[i] < input_2[i] || input_2[i] == NaN || input_2[i] == missing_input
                output_matrix[i] = input_1[i]
            elseif input_1[i] > input_2[i] || input_1[i] == NaN || input_1[i] == missing_input
                output_matrix[i] = input_2[i]
            elseif input_1[i] == input_2[i]
                output_matrix[i] = input_2[i]
            end
        end

    end

    return output_matrix
end

export and


"""
# Returns the probabilistic OR (also known as the algebraic sum)

    input_1: a fuzzy matrix

    input_2: a fuzzy matrix

    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

    missing_output: Define a 'missing_output' value to set the missing value in the fuzzified set. 

# Example
    
    ```julia-repl

        out = prob_or(input_1, input_2; missing_input, missing_output)

    ```
"""
function prob_or(input_1::AbstractMatrix{<:Real}, input_2::Matrix; missing_input::Any = nothing,
    missing_output::Any = nothing)

    output_prob_or::Matrix{eltype(input_1)} = zeros(eltype(input_1), size(input_1))

    for i in eachindex(output_prob_or)[2:end]
        if isnan(input_1[i]) && isnan(input_2[i])
            output_prob_or[i] = NaN
        elseif input_1[i] == missing_input && input_2[i] == missing_input
            output_matrix[i] = missing_output
        else
            output_prob_or = (input_1 .+ input_2) .- (input_1 .* input_2)
        end
    end

    return output_prob_or
end

export prob_or


"""
# Returns the probabilistic OR (also known as the algebraic sum) for datasets, which contain NaN

	input_1: a fuzzy matrix

    input_2: a fuzzy matrix

    output_prob_or: a matrix containing the results of the 'prob_or()' function

    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

    missing_output: Define a 'missing_output' value to set the missing value in the fuzzified set. 

# Example
    
    ```julia-repl

        out = prob_or_NaN(input_1, input_2; missing_input, missing_output)

    ```
"""
function prob_or_NaN(input_1::AbstractMatrix{<:Real}, input_2::AbstractMatrix{<:Real}, output_prob_or::Matrix; missing_input::Any = nothing,
    missing_output::Any = nothing)

    for i in eachindex(output_prob_or)
        if isnan(input_1[i]) && isnan(input_2[i])
            output_prob_or[i] = NaN
        elseif input_1[i] == missing_input && input_2[i] == missing_input
            output_matrix[i] = missing_output
        elseif isnan(input_1[i])
            output_prob_or[i] = input_2[i]
        elseif isnan(input_2[i])
            output_prob_or[i] = input_1[i]
        else
            output_prob_or
        end
    end
    return output_prob_or
end

export prob_or_NaN
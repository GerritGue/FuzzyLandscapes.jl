
"""
# Defuzzification

## Input Raster to fuzzy raster without export

    input - your input data,
            for example your location of tif file "../example.tif"

    method - choose your method of membership function
             for further information look into membership_functions.jl
             possibilities:  :Generalised_bell
                             :Trapezoidal
                             :Triangular
                             :Gaussian
                             :Gaussian2
                             :Sigmoid
                             :D_Sigmoid
                             :Z
                             :S
                             :PI

    x1,x2,x3,x4 - parameters of membership function
                  for further information look into membership_functions.jl

    defuzzy_method - choose your method of defuzzification
                     for further information look into centroid_bisector_lom_som_mom.jl
                     possibilities:  :Centroid
                                     :Bisector
                                     :Mom
                                     :Som
                                     :Lom

    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

    missing_output: Define a 'missing_output' value to set the missing value in the fuzzified set. 

"""

function defuzzification(input::String, method::Symbol, defuzzy_method::Symbol,
                         x1::Any = nothing, x2::Any = nothing, x3::Any = nothing,
                         x4::Any = nothing; missing_input::Any = nothing, missing_output::Any = nothing)

    input_matrix, input_raster = FuzzyLandscapes.raster_to_array(input)

    fuzzy_matrix::Matrix{eltype(input)} = zeros(eltype(input), size(input))
    
    fuzzified_matrix = FuzzyLandscapes.fuzzification(input_matrix, fuzzy_matrix, method, x1,
                                                    x2, x3, x4; missing_input, missing_output)

    defuzzy_output = defuzzy(input_matrix, fuzzified_matrix, defuzzy_method; missing_input)

    if !isempty(fuzzified_matrix)

        fuzzified_matrix_to_raster = FuzzyLandscapes.array_to_raster(fuzzified_matrix,
                                                                    input_raster; missing_input, missing_output)

        return fuzzified_matrix_to_raster, defuzzy_output

    else
        @error "Defuzzification unsuccessful. Possible reasons are types in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

"""
# Defuzzification

## Input matrix to fuzzy matrix without export

    input - your input data,
            for example your location of tif file "../example.tif"

    method - choose your method of membership function
             for further information look into membership_functions.jl
             possibilities:  :Generalised_bell
                             :Trapezoidal
                             :Triangular
                             :Gaussian
                             :Gaussian2
                             :Sigmoid
                             :D_Sigmoid
                             :Z
                             :S
                             :PI

    x1,x2,x3,x4 - parameters of membership function
                  for further information look into membership_functions.jl

    defuzzy_method - choose your method of defuzzification
                     for further information look into centroid_bisector_lom_som_mom.jl
                     possibilities:  :Centroid
                                     :Bisector
                                     :Mom
                                     :Som
                                     :Lom

    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

    missing_output: Define a 'missing_output' value to set the missing value in the fuzzified set. 
"""

function defuzzification(input::AbstractMatrix{<:Real}, method::Symbol, defuzzy_method::Symbol,
                         x1::Any = nothing, x2::Any = nothing, x3::Any = nothing,
                         x4::Any = nothing; missing_input::Any = nothing, missing_output::Any = nothing)

    fuzzy_matrix::Matrix{eltype(input)} = zeros(eltype(input), size(input))

    fuzzified_matrix = FuzzyLandscapes.fuzzification(input, fuzzy_matrix, method, x1, x2, x3,
                                                    x4; missing_input, missing_output)

    defuzzy_output = defuzzy(input, fuzzified_matrix, defuzzy_method; missing_input)

    if !isempty(fuzzified_matrix)

        return fuzzified_matrix, defuzzy_output

    else
        @error "Defuzzification of matrix unsuccessful. Possible reasons are typos in the method parameter, incorrect number of input points for the membership functions or corrupt input data."
    end
end

export defuzzification

"""
# Defuzzification

## This function is part of the 'defuzzification()'-function

    input: your input data, for example your location of tif file "../example.tif"

    fuzzified_matrix: A fuzzified matrix

    defuzzy_method - choose your method of defuzzification
                     for further information look into centroid_bisector_lom_som_mom.jl
                     possibilities:  :Centroid
                                     :Bisector
                                     :Mom
                                     :Som
                                     :Lom

    missing_input: Define a 'missing_input' value, if you have a numeric 'missing' in your input data.

    missing_output: Define a 'missing_output' value to set the missing value in the fuzzified set. 
"""

function defuzzy(input::AbstractMatrix{<:Real}, fuzzified_matrix::AbstractMatrix{<:Real}, defuzzy_method::Symbol; missing_input::Any = nothing)
    
    if defuzzy_method == :Centroid
        defuzzy_output = FuzzyLandscapes.centroid(input, fuzzified_matrix; missing_input)
    elseif defuzzy_method == :Mom
        defuzzy_output = FuzzyLandscapes.mom(input, fuzzified_matrix)
    elseif defuzzy_method == :Som
        defuzzy_output = FuzzyLandscapes.som(input, fuzzified_matrix)
    elseif defuzzy_method == :Lom
        defuzzy_output = FuzzyLandscapes.lom(input, fuzzified_matrix)
    elseif defuzzy_method == :Bisector
        defuzzy_output = FuzzyLandscapes.bisector(input, fuzzified_matrix; missing_input)
    end

    return defuzzy_output
end

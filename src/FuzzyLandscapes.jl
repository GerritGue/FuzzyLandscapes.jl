module FuzzyLandscapes

## dependencies
import ArchGDAL
import Interpolations
import Statistics
import GeoArrays
import Distances
import Test

## core scripts

include("Core/fuzzy_rule_based_system.jl")
include("Core/fuzzy_connection.jl")
include("Core/fuzzy_evaluation.jl")
include("Core/fuzzification.jl")
include("Core/fuzzy_membership_functions.jl")

include("Core/defuzzification.jl")
include("Core/defuzzification_methods.jl")

include("Core/fuzzy_inference.jl")


include("Core/export_raster.jl")

## helper functions

include("HelperFunctions/calculate_euclidean_distance.jl")

end
